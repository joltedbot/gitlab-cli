# gitlab-cli

A command line tool to manage projects in gitlab.

## Installation

Run the following to generate a jar files under the directory *./target/uberjar/*
> $> lein uberjar

This produces 2 jar files.  The file labeled "standalone" is the one you want.

Place the jar file somewhere accessible and then run with the commmands below.

## Usage

I recommend creating a shell script wrapper around it to make it easier to run.

Something like this works at minimum:

```
#!/bin/bash
java -jar gitlab-cli-standalone.jar $1 $2 $3
```


## Options

```
  -l, --list                       List Projects
  -e, --members PROJECT-ID         List the members of a project
  -s, --search PROJECT             Search for a given project name fragment
  -c, --clone PROJECT-ID           Return the git clone line for specified numeric project ID
  -r, --registry PROJECT-ID        Get the contents of the container registry for the specified numeric project id
  -u, --runners                    List your private runners
  -g, --groups                     List your groups
  -o, --groupmembers GROUP-ID      List the members of a group
  -p, --groupprojects GROUP-ID     List a groups projects
  -a, --share PROJECT-ID GROUP-ID  Share an existing project with a specific group.
  -m, --merge                      List open merge requests
  -n, --create PROJECT [GROUP-ID]  Create a new project. Optionally provide the ID of a group to create the project in.
  -v, --version
  -h, --help


```

## Examples

> $> java -jar gitlab-cli.jar -l

> $> java -jar gitlab-cli.jar --create my-new-project

> $> java -jar gitlab-cli.jar -r 123456 

> $> java -jar gitlab-cli.jar --merge 

...

### Bugs

...

### Release Notes
*v1.5.3* - Updated the nested loops used to parse json objects from the API to specter library selects.



## License

Copyright © 2020 Dave White

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
