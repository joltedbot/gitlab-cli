(ns gitlab-cli.print
  (:require [com.rpl.specter :as specter :refer [ALL MAP-KEYS MAP-VALS submap]]))

(defn listProjects [apiResponseList]
  (try
    (println "----------------------------------")
    (println "   ID         Project")
    (println "----------------------------------")
    (doseq [project (specter/select [ALL ALL (submap [:id :name])] apiResponseList)]
      (println (get project :id) " - " (get project :name)))
    (println "----------------------------------")
    (catch Exception e
      (println (str "Error requesting project list: \n\t" (.getMessage e) " " (get (ex-data e) :reason-phrase))))))


(defn listProjectMembers [apiResponseList]
  (try
    (println "-------------------------------------------------------------------")
    (println "   ID        username     Name")
    (println "-------------------------------------------------------------------")
    (doseq [member (specter/select [ALL ALL (submap [:id :name :username])] apiResponseList)]
      (println (get member :id) " - "
               (get member :username) " - "
               (get member :name)))
    (println "-------------------------------------------------------------------")
    (catch Exception e
      (println (str "Error requesting project members requests: \n\t" (.getMessage e) " " (get (ex-data e) :reason-phrase))))))


(defn listRunners [apiResponseList]
  (try
    (println "------------------------------------------------------------------")
    (println "ID           IP              Active   Online       Runner")
    (println "------------------------------------------------------------------")
    (doseq [runner (specter/select [ALL ALL (submap [:id :ip_address :active :online :description])] apiResponseList)]
      (println (get runner :id) " - "
               (get runner :ip_address) " - "
               (get runner :active) " - "
               (get runner :online) " - "
               (get runner :description)))
    (println "------------------------------------------------------------------")
    (catch Exception e
      (println (str "Error requesting project member list: \n\t" (.getMessage e) " " (get (ex-data e) :reason-phrase))))))

(defn listGroups [apiResponseList]
  (try
    (println "-------------------------------------------------------------")
    (println "   ID       Group         Visibility             URL")
    (println "-------------------------------------------------------------")
    (doseq [group (specter/select [ALL ALL (submap [:id :name :visibility :web_url])] apiResponseList)]
      (println (get group :id) " - "
               (get group :name) " - "
               (get group :visibility) " - "
               (get group :web_url)))
    (println "-------------------------------------------------------------")
    (catch Exception e
      (println (str "Error requesting group member list: \n\t" (.getMessage e) " " (get (ex-data e) :reason-phrase))))))

(defn listGroupMembers [apiResponseList]
  (try
    (println "-------------------------------------------------------------------")
    (println "   ID        username     Name")
    (println "-------------------------------------------------------------------")
    (doseq [member (specter/select [ALL ALL (submap [:id :name :username])] apiResponseList)]
      (println (get member :id) " - "
               (get member :username) " - "
               (get member :name)))
    (println "-------------------------------------------------------------------")
    (catch Exception e
      (println (str "Error requesting group member requests: \n\t" (.getMessage e) " " (get (ex-data e) :reason-phrase))))))

(defn listGroupProjects [apiResponseList]
  (try
    (println "----------------------------------")
    (println "   ID         Project")
    (println "----------------------------------")
    (doseq [project (specter/select [ALL ALL (submap [:id :name])] apiResponseList)]
      (println (get project :id) " - "
               (get project :name)))
    (println "----------------------------------")
    (catch Exception e
      (println (str "Error requesting group project list: \n\t" (.getMessage e) " " (get (ex-data e) :reason-phrase))))))

(defn listContainerRegistry [apiResponseList]
  (try
    (println "----------------------------------------------------------------------------")
    (println "   Name \t      Path")
    (println "----------------------------------------------------------------------------")
    (doseq [registry (specter/select [ALL ALL (submap [:name :location])] apiResponseList)]
      (println (get registry :name) " \t- "
               (get registry :location)))
    (println "----------------------------------------------------------------------------")
    (catch Exception e
      (println (str "Error requesting registry list: \n\t" (.getMessage e) " " (get (ex-data e) :reason-phrase))))))

(defn listProjectPipelines [apiResponseList]
  (try
    (println "------------------------------------------------------------------------------")
    (println "   ID         status        ref              Last Update")
    (println "------------------------------------------------------------------------------")
    (doseq [pipeline apiResponseList]
          (println (get pipeline :id) " - " (get pipeline :status) " - " (get pipeline :ref) " - " (get pipeline :updated_at)))
    (println "------------------------------------------------------------------------------")
    (catch Exception e (println (str "Error searching for projects pipelines: \n\t" (.getMessage e) " " (get (ex-data e) :reason-phrase))))))


(defn searchProject [apiResponseList]
  (try
    (println "----------------------------------")
    (println "   ID         Project")
    (println "----------------------------------")
    (doseq [project (specter/select [ALL ALL (submap [:id :name])] apiResponseList)]
      (println (get project :id) " - " (get project :name)))
    (println "----------------------------------")
    (catch Exception e (println (str "Error searching for projects: \n\t" (.getMessage e) " " (get (ex-data e) :reason-phrase))))))

(defn projectCloneString [apiResponse]
  (try
    (println (str "git clone " (:ssh_url_to_repo apiResponse)))
    (catch Exception e
      (println (str "Error requesting registry list: \n\t" (.getMessage e) " " (get (ex-data e) :reason-phrase))))))