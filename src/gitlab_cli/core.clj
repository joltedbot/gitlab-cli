(ns gitlab-cli.core
  (:require [gitlab-cli.cli :as cli])
  (:gen-class))

(def appVersion "v1.5.4")

(defn -main
  [& args]
  (cli/actionCLIParameters args appVersion))