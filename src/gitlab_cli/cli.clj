(ns gitlab-cli.cli
  (:require [clojure.tools.cli :refer [parse-opts]]
            [gitlab-cli.api :as api]
            [gitlab-cli.print :as print]
            [environ.core :refer [env]]))

(def cliOptions
  [["-l" "--list" "List Projects"]
   ["-e" "--members PROJECT-ID" "List the members of a project"
    :parse-fn #(Integer/parseInt %)
    :validate [#(integer? %) "Project ID must be a number"]]
   ["-s" "--search PROJECT" "Search for a given project name fragment"]
   ["-c" "--clone PROJECT-ID" "Return the git clone line for specified numeric project ID"
    :parse-fn #(Integer/parseInt %)
    :validate [#(integer? %) "Project ID must be a number"]]
   ["-r" "--registry PROJECT-ID" "Get the contents of the container registry for the specified numeric project id"
    :parse-fn #(Integer/parseInt %)
    :validate [#(integer? %) "Project ID must be a number"]]
   ["-i" "--pipelines PROJECT-ID" "List the last 10 pipeline runs for the specified numeric project id"
    :parse-fn #(Integer/parseInt %)
    :validate [#(integer? %) "Project ID must be a number"]]
   ["-u" "--runners" "List your private runners"]
   ["-g" "--groups" "List your groups"]
   ["-o" "--groupmembers GROUP-ID" "List the members of a group"
    :parse-fn #(Integer/parseInt %)
    :validate [#(integer? %) "Group ID must be a number"]]
   ["-p" "--groupprojects GROUP-ID" "List a groups projects"
    :parse-fn #(Integer/parseInt %)
    :validate [#(integer? %) "Group ID must be a number"]]
   ["-j" "--jobs PROJECT-ID PIPELINE-ID" "List the jobs for a specific project pipeline"
    :parse-fn #(Integer/parseInt %)
    :validate [#(integer? %) "Project ID must be a number"]]
   ["-a" "--share PROJECT-ID GROUP-ID" "Share an existing project with a specific group."
    :parse-fn #(Integer/parseInt %)
    :validate [#(integer? %) "Project ID must be a number"]]
   ["-m" "--merge" "List open merge requests"]
   ["-n" "--create PROJECT [GROUP-ID]" "Create a new project. Optionally provide the ID of a group to create the project in."
    :parse-fn #(str %)
    :validate [#(string? %) "Group ID must be a number"]]
   ["-v" "--version"]
   ["-h" "--help"]])


(defn validateInputParameter [parameter allowNIL]
  (try
    (if (nil? parameter)
      (if allowNIL
        nil
        (throw (Exception. "Parameter can not be empty!")))
      (Integer/parseInt parameter))
    (catch Exception e
      (println (str "Invalid input: \n\t" (.getMessage e) " " (get (ex-data e) :reason-phrase)))
      (System/exit 1))))


(defn actionCLIParameters [args appVersion]
  (let [params (parse-opts args cliOptions)]
    (if (= (count (get params :errors)) 0)
      (let [action (first (get params :options))]
        (cond
          (= (first action) :list) (print/listProjects
                                     (api/getAPIWithPages
                                       (env :gitlabtoken)
                                       (str (env :gitlaburi) "/projects")
                                       "owned=true"))
          (= (first action) :members) (print/listProjectMembers
                                        (api/getAPIWithPages
                                          (env :gitlabtoken)
                                          (str (env :gitlaburi) "/projects/" (nth action 1) "/members")
                                          nil))
          (= (first action) :search) (print/searchProject
                                       (api/getAPIWithPages
                                         (env :gitlabtoken)
                                         (str (env :gitlaburi) "/projects")
                                         (str "owned=true&search=" (nth action 1))))
          (= (first action) :clone) (print/projectCloneString
                                      (api/getAPIWithoutPages
                                        (env :gitlabtoken)
                                        (str (env :gitlaburi) "/projects/" (nth action 1))))
          (= (first action) :registry) (print/listContainerRegistry
                                         (api/getAPIWithPages
                                           (env :gitlabtoken)
                                           (str (env :gitlaburi) "/projects/" (nth action 1) "/registry/repositories")
                                           nil))
          (= (first action) :pipelines) (print/listProjectPipelines
                                          (api/getAPIWithoutPages
                                            (env :gitlabtoken)
                                            (str (env :gitlaburi) "/projects/" (nth action 1) "/pipelines")))
          (= (first action) :runners) (print/listRunners
                                        (api/getAPIWithPages
                                          (env :gitlabtoken)
                                          (str (env :gitlaburi) "/runners")
                                          nil))
          (= (first action) :groups) (print/listGroups
                                       (api/getAPIWithPages
                                         (env :gitlabtoken)
                                         (str (env :gitlaburi) "/groups")
                                         nil))
          (= (first action) :groupmembers) (print/listGroupMembers
                                             (api/getAPIWithPages
                                               (env :gitlabtoken)
                                               (str (env :gitlaburi) "/groups/" (nth action 1) "/members")
                                               nil))
          (= (first action) :groupprojects) (print/listGroupProjects
                                              (api/getAPIWithPages
                                                (env :gitlabtoken)
                                                (str (env :gitlaburi) "/groups/" (nth action 1) "/projects")
                                                nil))
          (= (first action) :jobs) (api/listPipelineDetails (env :gitlabtoken)
                                                            (str (env :gitlaburi) "/projects/" (nth action 1) "/pipelines/"
                                                                 (validateInputParameter (first (get params :arguments)) false) "/jobs"))
          (= (first action) :share) (api/shareProject (env :gitlabtoken) (env :gitlaburi) (nth action 1)
                                                      (validateInputParameter (first (get params :arguments)) false))
          (= (first action) :merge) (api/listMergeRequests (env :gitlabtoken) (env :gitlaburi))
          (= (first action) :create) (api/createProject (env :gitlabtoken) (env :gitlaburi) (nth action 1)
                                                        (validateInputParameter (first (get params :arguments)) true))
          (= (first action) :version) (println "gitlab-cli" appVersion)
          (= (first action) :help) (println (get params :summary))
          :else (println (get params :errors))))
      (println (get params :summary)))))
