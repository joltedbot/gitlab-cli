(ns gitlab-cli.api
  (:require [environ.core :refer [env]]
            [clj-http.client :as client]
            [clojure.data.json :as json]))

(defn- getAPI [token apiURI]
  (try
    (let [httpResponse (client/get apiURI {:headers {"PRIVATE-TOKEN" token}})]
      {:body (json/read-str (:body httpResponse) :key-fn keyword) :headers (:headers httpResponse)})
    (catch Exception e
      (println (str "Error making API call: \n\t" (.getMessage e) " " (get (ex-data e) :reason-phrase))))))

(defn getAPIWithoutPages [token apiURI]
  (try
    (let [apiResponse (getAPI token apiURI)]
      (:body apiResponse))
    (catch Exception e
      (println (str "Error making API call: \n\t" (.getMessage e) " " (get (ex-data e) :reason-phrase))))))

(defn getAPIWithPages [token apiURI uriParameters]
  (try
    (loop [page 1 total-pages 1 bodyAggregator []]
      (if (> page total-pages)
        bodyAggregator
        (let [httpResponse (getAPI token
                             (str apiURI "?per_page=100&" (if (= uriParameters nil) (str "page=" page) (str uriParameters "&" "page=" page))))]
          (let [responseBody (:body httpResponse)]
            (recur (inc page) (Integer/parseInt (get (:headers httpResponse) :X-Total-Pages)) (conj bodyAggregator responseBody))))))
    (catch Exception e
      (println (str "Error making paginated API call: \n\t" (.getMessage e) " " (get (ex-data e) :reason-phrase))))))


(defn listMergeRequests [token gitlabURI]
  (try
    (let [httpResponse (getAPI token (str gitlabURI "/merge_requests?state=opened"))]
      (println "---------------------------------------")
      (println "   ID        Project     Merge Request")
      (println "---------------------------------------")
      (doseq [request (:body httpResponse)]
        (let [project (getAPI token (str gitlabURI "/projects/" (get request :project_id)))]
          (println (get request :id) " - "
                   (get project :name) " - "
                   (get request :title))))
      (println "---------------------------------------"))
    (catch Exception e
      (println (str "Error requesting merge requests: \n\t" (.getMessage e) " " (get (ex-data e) :reason-phrase))))))

(defn listPipelineDetails [token gitlabURI]
  (try
    (let [httpResponse (getAPI token gitlabURI)]
      (println "------------------------------------------------------------------------------------------")
      (println " Status      Stage        Name        Duration        Runner")
      (println "------------------------------------------------------------------------------------------")
      (doseq [request (:body httpResponse)]
        (println (get request :status) " - "
                 (get request :stage) " - "
                 (get request :name) " - "
                 (get request :duration) "\bs - "
                 (:description (get request :runner))))
      (println "------------------------------------------------------------------------------------------"))
    (catch Exception e
      (println (str "Error requesting merge requests: \n\t" (.getMessage e) " " (get (ex-data e) :reason-phrase))))))

(defn shareProject [token gitlabURI projectID groupID]
  (try
    (client/post (str gitlabURI "/projects/" projectID "/share") {:headers     {"PRIVATE-TOKEN" token}
                                                                   :form-params {:group_id     groupID
                                                                                 :group_access 30}})
    (println "The project has been shared with the specified group")
    (catch Exception e (println (str "Error sharing project: \n\t" (.getMessage e) " " (get (ex-data e) :reason-phrase))))))


(defn createProject [token gitlabURI project groupID]
  (try
    (let [httpResponse
          (client/post (str gitlabURI "/projects") {:headers {"PRIVATE-TOKEN" token}
                                                    :form-params {:name project
                                                                  :visibility "private"
                                                                  :namespace_id groupID}})]
      (let [httpResponseBody (json/read-str (:body httpResponse) :key-fn keyword)]
        (println (str "git clone " (get httpResponseBody :ssh_url_to_repo)))))
    (catch Exception e (println (str "Error creating project: \n\t" (.getMessage e) " " (get (ex-data e) :reason-phrase))))))


