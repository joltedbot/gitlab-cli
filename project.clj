(defproject gitlab-cli "v1.5.4"
  :description "A command line tool to manage projects in gitlab."
  :url ""
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [clj-http "3.12.3"]
                 [org.clojure/data.json "2.4.0"]
                 [environ "1.2.0"]
                 [ring/ring-mock "0.4.0"]
                 [org.clojure/tools.cli "1.0.219"]
                 [com.rpl/specter "1.1.4"]]
  :plugins [[lein-environ "1.2.0"]]
  :main ^:skip-aot gitlab-cli.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
